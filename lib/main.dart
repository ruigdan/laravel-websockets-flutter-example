import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:pusher_client/pusher_client.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late PusherClient pusher;
  late Channel channel;

  String channelName = 'private-booking.161'; // private channel for testing

  bool pusherSubscriptionSucceeded = false;

  @override
  void initState() {
    super.initState();

    String pusherKey = 'axsszeVV';
    String pusherHost = '136gateway.grt-team.com';
    int pusherPort = 6103;
    String pusherAuthUrl =
        'https://136gateway.grt-team.com/api/broadcasting/auth';

    String authToken = '679|61YQSsq3QJbNa2DrOtXUyecE5USt4SDhzsOsxtVV'; // test user auth token

    pusher = PusherClient(
      pusherKey,
      PusherOptions(
        host: pusherHost,
        wssPort: pusherPort,
        encrypted: true,
        auth: PusherAuth(
          pusherAuthUrl,
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer $authToken',
          },
        ),
      ),
      enableLogging: true,
      autoConnect: false,
    );

    pusher.connect();

    pusher.onConnectionStateChange((state) {
      log("Connection previousState: ${state?.previousState}, currentState: ${state?.currentState}");
    });

    pusher.onConnectionError((error) {
      log("Connection error (${error?.code}): ${error?.message}");
      log("Connection exception: ${error?.exception}");
    });

    subscribeToChannel();
  }

  void subscribeToChannel() {
    channel = pusher.subscribe(channelName);

    channel.bind("pusher:subscription_succeeded", (PusherEvent? event) {
      pusherSubscriptionSucceeded = true;
    });

    String channelEventName = 'Booking';

    channel.bind(channelEventName, (PusherEvent? event) {
      log("$channelEventName event: ${event?.data}");
    }).onError((error, stackTrace) => log("$channelEventName event error: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Laravel Websockets App'),
        ),
        body: Center(
            child: Column(
          children: [
            ElevatedButton(
              child: Text("Subscribe channel: $channelName"),
              onPressed: () {
                subscribeToChannel();
              },
            ),
            ElevatedButton(
              child: Text("Unsubscribe channel: $channelName"),
              onPressed: () async {
                await pusher.unsubscribe(channelName);
                pusherSubscriptionSucceeded = false;
              },
            ),
            ElevatedButton(
              child: const Text("Trigger client event"),
              onPressed: () {
                if (pusherSubscriptionSucceeded) {
                  channel.trigger("client-grt.test", {"name": "GRT test"});
                }
              },
            ),
          ],
        )),
      ),
    );
  }
}
